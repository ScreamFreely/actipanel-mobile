import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

import Welcome from '@/components/Welcome'
import Splash from '@/components/Splash'
import Main from '@/components/Main'

import About from '@/components/About'

import Requests from '@/components/Requests'
import Videos from '@/components/Videos'
import Video from '@/components/Video'

import Login from '@/components/Login'

import store from '../store' // your vuex store 

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    console.log('is authed')
    return
  }
  next('/')
}


Vue.use(Router)
Vue.use(Meta)

export default new Router({
    mode: 'history',
    base: __dirname,
    routes: [
	    {
	      	path: '/',
	      	name: 'Welcome',
			component: Splash,
			children: [
				{
				    path: '',
				    components: {
					header: Welcome,
					main: Main
				    }
				},
				{
				    path: '/about',
				    components: {
					header: Welcome,
					main: About
				    }
				},
				{
				    path: '/requests',
				    components: {
					header: Welcome,
					main: Requests
				    }
				},
				{
				    path: '/login',
				    components: {
					header: Welcome,
					main: Login
				    },
				    beforeEnter: ifNotAuthenticated,
				},
			]
	    },

	    {
		    path: '/videos',
		    name: 'Videos',
			component: Splash,
			children: [
				{
				    path: '/videos',
				    components: {
					header: Welcome,
					main: Videos
				    }
				},
				{
				    path: '/video/:slug',
				    components: {
					header: Welcome,
					main: Video
				    }
				},
			]
	    },
    ]

})
