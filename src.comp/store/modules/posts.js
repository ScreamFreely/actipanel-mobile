// import shop from '../../api/shop'
import axios from 'axios'

import api from '../api'


// initial state
const state = {
  all: [],
  posts: [],
  videos: [],
  post: {},
  video: {},
  parcel: '',
  social: [],
  sponsors: [],
  vidasks: [],
}

// getters
const getters = {
  newPost: state => state.newPost,
  post: state => state.post,
  posts: state => state.posts,
  video: state => state.video,
  videos: state => state.videos,
  vidasks: state => state.vidasks,
  sponsors: state => state.sponsors,
  social: state => state.social,
  all: state => state.all,
}

// actions
const actions = {
  getSocial({ commit }) {
    console.log('getting social 1', api.apiBase + 'api/imenu')
    axios.get(api.apiBase + 'api/imenu')
      .then((response)=>{
        console.log('Social response', response)
        this.commit('posts/setSocial', response.data.results)
      })
      .catch((err, response) =>{
        console.log('err', err)
      })
    // console.log('got posts')
  },
  getSponsors({ commit }) {
    console.log('getting sponsors', api.apiBase + 'api/sponsors')
    axios.get(api.apiBase + 'api/sponsors')
      .then((response)=>{
        console.log('Sponsors response', response)
        this.commit('posts/setSponsors', response.data.results)
      })
    // console.log('got posts')
  },
  getPosts ({ commit }) {
    console.log('getting posts', api.apiBase)
    axios.get(api.apiBase + 'api/iposts')
      .then((response)=>{
        console.log('Post response', response)
        this.commit('posts/setPosts', response.data.results)
      })
    // console.log('got posts')
  },
  getPost ({ commit }, num) {
    console.log('getting post', api.apiBase)
    axios.get(api.apiBase + 'api/ipost/' + num)
      .then((response)=>{
        console.log('Post response', response)
        this.commit('posts/setPost', response.data.results[0])
      })
      .catch((err, response) =>{
        console.log('err', err)
        axios.get(api.apiBase + 'api/iposts')
          .then((response)=>{
            console.log('Catch Post response', response)
            this.commit('posts/setPost', response.data.results)
          }).catch((err) => {console.log(err)})
      })
    // console.log('got posts')
  },
  searchVideos ({ commit }, searchTerm) {
    console.log('searching videos')
    const params = {'q': searchTerm}
    console.log(params)
    axios.get(api.apiBase + 'api/searchvids', {params})
      .then((response)=>{
        console.log('Video response', response)
        this.commit('posts/setVideos', response.data.results)
      })
    // console.log('got projects')
  },
  getVideos ({ commit }) {
    // console.log('getting projects')
    axios.get(api.apiBase + 'api/ivids')
      .then((response)=>{
        console.log('Video response', response)
        this.commit('posts/setVideos', response.data.results)
      })
    // console.log('got projects')
  },
  getVideo ({ commit }, slug) {
    console.log('getting video', slug)
    axios.get(api.apiBase + 'api/ivid/' + slug )
      .then((response)=>{
        console.log('Video response', response)
        this.commit('posts/setVideo', response.data.results[0])
      })
    // console.log('got projects')
  },
  getRequests ({ commit }) {
    // console.log('getting projects')
    axios.get(api.apiBase + 'api/ividask')
      .then((response)=>{
        console.log('Requets response', response)
        this.commit('posts/setRequests', response.data.results)
      })
    // console.log('got projects')
  },
  requestVideo ({ commit }, vidAsk) {
    console.log('getting video', vidAsk)
    axios.post(api.apiBase + 'api/ividask', vidAsk)
      .then((response)=>{
        console.log('Video response', response)
      })
      .catch(error => {
        console.log('error')
    })
    // console.log('got projects')
  },
}

// mutations
const mutations = {
  reset (s) {
   const initial = state()
    Object.keys(initial).forEach(key => { s[key] = initial[key] })
  },
  setSponsors(state, sponsors){
      state.sponsors = sponsors
  },
  setSocial(state, social){
      state.social = social
  },
  setPosts(state, posts){
    state.posts = posts
    state.parcel = 'posts'
  },
  setPost(state, post){
    state.post = post
  },
  setVideos(state, videos){
    state.videos = videos
  },
  setRequests(state, vidAsks){
    state.vidasks = vidAsks
  },
  setVideo(state, video){
    state.video = video
  },
  setParcel(state, parcel){
    state.parcel = parcel.parcel
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}