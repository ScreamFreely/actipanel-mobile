import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from '../actions/user'
// import apiCall from '../../utils/api'
import Vue from 'vue'
import { AUTH_LOGOUT } from '../actions/auth'

import axios from 'axios'

import api from '../api'

const state = { 
  status: '', 
  profile: {},
  username: localStorage.getItem('user-name')
}

const getters = {
  getProfile: state => state.user.profile,
  getUsername: state => state.user.username,
  isProfileLoaded: state => !!state.profile.name,
}

const actions = {
  [USER_REQUEST]: ({commit, dispatch}, user) => {
    commit(USER_REQUEST)
    console.log('how is tihs ging?', user)
    axios.get(api.apiBase + 'api/user/' + user.username + '/')
    // axios({url: 'http://localhost:8000/api/user/' + user.username + '/', method: 'GET'})
      .then(resp => {
        console.log('did this happen?', resp.data.results[0])
        const user = resp.data.results[0]
        localStorage.setItem('user-name', user.username)
        localStorage.setItem('user-type', user.user_type)
        commit(USER_SUCCESS, user)
      })
      .catch(resp => {
        commit(USER_ERROR)
        // if resp is unauthorized, logout, to
        console.log('user get failed')
        dispatch(AUTH_LOGOUT)
      })
  },
}

const mutations = {
  [USER_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [USER_SUCCESS]: (state, resp) => {
    state.status = 'success'
    Vue.set(state, 'profile', resp)
  },
  [USER_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.profile = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
}