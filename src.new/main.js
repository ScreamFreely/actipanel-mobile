// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'

import router from './router'

import store from './store'

import axios from 'axios'
import VueAxios from 'vue-axios'
import  jwt_decode from 'jwt-decode'
import Vuex from 'vuex'

import VueMarkdown from 'vue-markdown'

import ElementUI from 'element-ui'
// import vueResource from 'vue-resource'

import App from './App'
import Vue from 'vue'

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

Vue.use(ElementUI, { locale })
Vue.use(Vuex)

Vue.use(VueAxios, axios)
Vue.use(require('vue-moment'))

const token = localStorage.getItem('user-token')

if (token) {
  axios.defaults.headers.common['Authorization'] = 'JWT ' + token
}

/* eslint-disable no-new */
new Vue({
  store,
  router,
  template: '<router-view />'
}).$mount('#app')
   
