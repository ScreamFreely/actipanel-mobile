import Vue from 'vue'
import Router from 'vue-router'

import Main from '@/components/Main'
import Requests from '@/components/Requests'
import Videos from '@/components/Videos'
import Video from '@/components/Video'
import About from '@/components/About'
import English from '@/components/English'



import store from '../store' 

Vue.use(Router)


export default new Router({
  mode: 'hash',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    },
    {
      path: '/about',
      name: 'About',
      component: About,
    },
    {
      path: '/requests',
      name: 'Requests',
      component: Requests,
    },
    {
      path: '/videos',
      name: 'Videos',
      component: Videos,
    },
    {
      path: '/video/:slug',
      name: 'Video',
      component: Video,
    },
    {
      path: '/english',
      name: 'English',
      component: English,
    },
    {
      path: '/main',
      name: 'Main',
      component: Main,
    },
  ]
})
